<%@ page import ="java.util.*" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
	<div class="page-header">
		<h1>
		    Available Brands
		</h1>
		<%
		List result= (List) request.getAttribute("brands");
		Iterator it = result.iterator();
		out.println("<br>At LCBO, We have <br><br>");
		while(it.hasNext()){
		out.println(it.next()+"<br>");
		}
		%>
		<br><a href="http://analytics.mindextent.com:8000/yuzhang/">Go Back Main Page</a>
	</div>
</div>
</body>
</html>